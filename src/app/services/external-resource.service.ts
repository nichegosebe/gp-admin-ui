import {Injectable} from "@angular/core";

@Injectable()
export class ExternalResourceService {

  private static readonly defaultStyleAttributes = {
    type: 'text/css',
    rel: 'stylesheet'
  };

  private static readonly defaultScriptAttributes = {
    type: 'text/javascript'
  };

  /**
   * Список уже загруженных на страницу скриптов
   *
   * @type {Array}
   */
  private loadedScripts: string[] = [];

  /**
   * Список уже загруженных на страницу стилей
   *
   * @type {Array}
   */
  private loadedStyles: string[] = [];

  /**
   * Список уже загруженных изображений
   *
   * @type {Array}
   */
  private loadedImages: string[] = [];

  /**
   * Загрузка сразу нескольких стилей
   *
   * @param styles
   * @returns {Promise<any>}
   */
  public loadStyles(styles: {href: string, optionalAttributes?: any}[]): Promise<any> {
    return Promise.all(styles.map((style: {href: string, optionalAttributes?: any}) => {
      return this.loadStyle(style.href, style.optionalAttributes || {});
    }));
  }

  /**
   * Загрузка сразу нескольких скриптов
   *
   * @param scripts
   * @returns {Promise<any[]>}
   */
  public loadScripts(scripts: {src: string, optionalAttributes?: any}[]): Promise<any> {
    return Promise.all(scripts.map((script: {src: string, optionalAttributes?: any}) => {
      return this.loadScript(script.src, script.optionalAttributes || {});
    }));
  }

  /**
   * Загрузка сразу нескольких изображений
   *
   * @param images
   * @returns {Promise<any[]>}
   */
  public loadImages(images: {src: string, optionalAttributes?: any}[]): Promise<any> {
    return Promise.all(images.map((image: {src: string, optionalAttributes?: any}) => {
      return this.loadImage(image.src, image.optionalAttributes || {});
    }))
  }

  /**
   * Загрузка внешнего CSS-файла. Вернет promise в случае успеха загрузки файла, будет вызван resolve(), иначе reject()
   *
   * @param href
   * @param optionalAttributes
   * @returns {Promise<any>}
   */
  public loadStyle(href: string, optionalAttributes?: any): Promise<any> {
    return new Promise((resolve, reject) => {
      if (this.loadedStyles.indexOf(href) !== -1) {
        resolve();
      } else {
        const link = document.createElement('link'),
          attributes = Object.assign(ExternalResourceService.defaultStyleAttributes, optionalAttributes || {});

        for (let name in attributes) {
          if (attributes.hasOwnProperty(name)) {
            link.setAttribute(name, attributes[name]);
          }
        }

        link.href = href;

        link.onload = () => {
          this.loadedStyles.push(href);
          resolve();
        };
        link.onerror = reject;

        document.head.appendChild(link);
      }
    });
  }

  /**
   * Загрузка внешнего JS-файла. Вернет promise в случае успеха загрузки файла, будет вызван resolve(), иначе reject()
   *
   * @param src
   * @param optionalAttributes
   * @returns {Promise<any>}
   */
  public loadScript(src: string, optionalAttributes?: any): Promise<any> {
    return new Promise((resolve, reject) => {
      if (this.loadedScripts.indexOf(src) !== -1) {
        resolve();
      } else {
        const script = document.createElement('script'),
          attributes = Object.assign(ExternalResourceService.defaultScriptAttributes, optionalAttributes || {});

        for (let name in attributes) {
          if (attributes.hasOwnProperty(name)) {
            script.setAttribute(name, attributes[name]);
          }
        }

        script.src = src;

        script.onload = () => {
          this.loadedScripts.push(src);
          resolve();
        };
        script.onerror = reject;

        document.body.appendChild(script);
      }
    });
  }

  /**
   * Загрузка изображения. Вернет promise в случае успеха загрузки файла, будет вызван resolve(), иначе reject()
   *
   * @param src
   * @param optionalAttributes
   * @returns {Promise<any>}
   */
  public loadImage(src: string, optionalAttributes?: any): Promise<any> {
    return new Promise((resolve, reject) => {
      if (this.loadedImages.indexOf(src) !== -1) {
        resolve();
      } else {
        const image = new Image(),
          attributes = optionalAttributes || {};

        for (let name in attributes) {
          image.setAttribute(name, attributes[name]);
        }

        image.onload = () => {
          this.loadedImages.push(src);
          resolve();
        };
        image.onerror = reject;

        image.src = src;
      }
    });
  }
}
