import {Injectable} from '@angular/core';

@Injectable()
export class HelperService {
  /**
   * Plural forms
   *
   * Use:
   *
   * declOfNum(count, ['найдена', 'найдено', 'найдены']);
   *
   * @see https://gist.github.com/realmyst/1262561#gistcomment-2032406
   *
   * @param n
   * @param titles
   * @returns {string}
   */
  public declOfNum(n: number, titles: string[]) {
    const cases = [2, 0, 1, 1, 1, 2];
    return titles[(n % 100 > 4 && n % 100 < 20) ? 2 : cases[(n % 10 < 5) ? n % 10 : 5]];
  }

  /**
   * Возвращает true если на странице выделен текст
   *
   * @returns {boolean}
   */
  public getSelectedText(): string {
    let result = '';

    if (window.getSelection) {
      result = window.getSelection().toString();
    }

    return result;
  }

  /**
   * Проверяет, является ли переданная строка валидным E-mail'ом
   * @param email
   * @returns {boolean}
   */
  public isValidEmail(email: string): boolean {
    return /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i
      .test(email);
  }

}
