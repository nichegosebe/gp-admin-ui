import {ElementRef, AfterContentChecked} from '@angular/core';

/**
 * Базовый абстрактный класс для компонентов
 * Добавляет свойство extraClass в котором описаны CSS-классы переданные в компонент
 */
export abstract class AbstractComponent implements AfterContentChecked {
    public static readonly CLASS_DELIMITER: string = ' ';

    public extraClass = '';

    constructor(protected el: ElementRef) {
    }

    ngAfterContentChecked() {
        if (this.el && this.el.nativeElement && this.el.nativeElement.className && !this.extraClass) {
            this.extraClass = this.el.nativeElement.className;
            this.el.nativeElement.className = '';
        }
    }

    /**
     * Возвращает массив CSS-классов переданных в компонент
     * @returns {string[]}
     */
    protected getExtraClasses(): string[] {
        return this.extraClass && this.extraClass.split
          ? this.extraClass.split(AbstractComponent.CLASS_DELIMITER).filter(item => item.length > 0)
          : [];
    }

    /**
     * Возвращает элемент компонента
     * @returns {ElementRef}
     */
    protected getElementRef() {
        return this.el;
    }
}
