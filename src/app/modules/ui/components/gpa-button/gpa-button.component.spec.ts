import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GpaButtonComponent } from './gpa-button.component';

describe('GpaButtonComponent', () => {
  let component: GpaButtonComponent;
  let fixture: ComponentFixture<GpaButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpaButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpaButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
