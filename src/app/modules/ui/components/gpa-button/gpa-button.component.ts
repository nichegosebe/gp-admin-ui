import {Component, Input, ElementRef} from '@angular/core';
import {AbstractComponent} from '../../../../abstract.component';

@Component({
  selector: 'gpa-button',
  templateUrl: './gpa-button.component.html',
  styleUrls: ['./gpa-button.component.scss']
})

export class GpaButtonComponent extends AbstractComponent {
  @Input() text: string = '';
  @Input() state: string = '';
  @Input() color: string = '';
  @Input() size: string = '';
  @Input() disabled: boolean = false;

  constructor(protected el: ElementRef) {
    super(el);
  }

}
