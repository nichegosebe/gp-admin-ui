import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GpaYandexMapComponent } from './gpa-yandex-map.component';

describe('GpaYandexMapComponent', () => {
  let component: GpaYandexMapComponent;
  let fixture: ComponentFixture<GpaYandexMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpaYandexMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpaYandexMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
