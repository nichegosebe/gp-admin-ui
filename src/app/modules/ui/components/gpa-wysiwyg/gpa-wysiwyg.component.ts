import {Component, ElementRef} from '@angular/core';
import {AbstractComponent} from '../../../../abstract.component';

@Component({
  selector: 'gpa-wysiwyg',
  templateUrl: './gpa-wysiwyg.component.html',
  styleUrls: ['./gpa-wysiwyg.component.css']
})
export class GpaWysiwygComponent extends AbstractComponent {

  constructor(protected el: ElementRef) {
    super(el);
  }

}
