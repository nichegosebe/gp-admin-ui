import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GpaWysiwygComponent } from './gpa-wysiwyg.component';

describe('GpaWysiwygComponent', () => {
  let component: GpaWysiwygComponent;
  let fixture: ComponentFixture<GpaWysiwygComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpaWysiwygComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpaWysiwygComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
