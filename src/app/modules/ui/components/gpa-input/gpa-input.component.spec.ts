import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GpaInputComponent } from './gpa-input.component';

describe('GpaInputComponent', () => {
  let component: GpaInputComponent;
  let fixture: ComponentFixture<GpaInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpaInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpaInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
