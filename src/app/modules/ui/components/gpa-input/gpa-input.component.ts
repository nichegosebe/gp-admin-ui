import {Component, Input, ElementRef, forwardRef} from '@angular/core';
import {AbstractComponent} from '../../../../abstract.component';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";

@Component({
    selector: 'gpa-input',
    templateUrl: './gpa-input.component.html',
    styleUrls: ['./gpa-input.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => GpaInputComponent),
            multi: true
        }
    ],
})

export class GpaInputComponent extends AbstractComponent implements ControlValueAccessor {
    @Input() title: string = '';
    @Input() type: string = '';
    @Input() inputName: string = '';
    @Input() value: string = '';
    @Input() placeholder: string = '';
    @Input() disabled: boolean = false;
    @Input() state: string = '';
    @Input() errorText: string;

    constructor(protected el: ElementRef) {
        super(el);
    }

    propagateChange = (_: any) => {
    };

    change(): void {
        this.propagateChange(this.value);
    }

    writeValue(value: string): void {
        this.value = value;
    }

    registerOnChange(fn: any): void {
        this.propagateChange = fn;
    }

    registerOnTouched(fn: any): void {
    }
}
