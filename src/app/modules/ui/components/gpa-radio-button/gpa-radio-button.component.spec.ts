import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GpaRadioButtonComponent } from './gpa-radio-button.component';

describe('GpaRadioButtonComponent', () => {
  let component: GpaRadioButtonComponent;
  let fixture: ComponentFixture<GpaRadioButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpaRadioButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpaRadioButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
