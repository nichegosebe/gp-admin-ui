import {AfterViewChecked, ChangeDetectorRef, Component, ElementRef, Input} from '@angular/core';
import {AbstractComponent} from '../../../../abstract.component';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
    selector: 'gpa-radio-button',
    templateUrl: './gpa-radio-button.component.html',
    styleUrls: ['./gpa-radio-button.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: GpaRadioButtonComponent,
            multi: true
        }
    ]
})

export class GpaRadioButtonComponent extends AbstractComponent implements AfterViewChecked, ControlValueAccessor {

    /**
     * Уникальное имя
     *
     * @type {string}
     */
    @Input() name: string;

    /**
     * Текст ошибки
     *
     * @type {string | boolean}
     */
    @Input() error: string | boolean = false;

    /**
     * Дисаблед
     *
     * @type {boolean}
     */
    @Input() disabled: boolean = false;

    /**
     * Значение по умолчанию
     */
    @Input() value: string | number | any;

    public options: any[] = [];

    constructor(protected el: ElementRef,
                private changeDetector: ChangeDetectorRef) {
        super(el);
    }

    ngAfterViewChecked() {
        const opts = this.getElementRef().nativeElement.querySelectorAll('option');
        if (opts.length > 0) {
            let needChanges = false;
            for (const option of opts) {
                const exists = this.options.find(f => f.value === option.value);
                if (!exists) {
                    this.options.push({value: option.value, text: option.text, selected: this.value === option.value});
                    if (option.selected) {
                        this.propagateChange(option.value);
                    }
                    needChanges = true;
                }
            }
            if (needChanges) {
                this.changeDetector.detectChanges();
            }
        }
    }

    writeValue(obj: string | number | any): void {
        this.options.forEach((item, index) => {
            this.options[index].selected = obj === item.value;
        });
    }

    registerOnChange(fn: any): void {
        this.propagateChange = fn;
    }

    registerOnTouched(fn: any): void {
    }

    propagateChange = (_: any) => {
    }

    setDisabledState(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }

    onChange(e, event) {
        if (!event.currentTarget.checked) {
            event.currentTarget.checked = true;
        } else {
            this.options.forEach((item, index) => {
                this.options[index].selected = e === item.value;
            });
        }
        this.propagateChange(e);
    }

}
