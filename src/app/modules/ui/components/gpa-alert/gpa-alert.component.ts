import {Component, ElementRef, EventEmitter, Input, Output, ViewChild} from '@angular/core';

import {AbstractComponent} from '../../../../abstract.component';

@Component({
  selector: 'gpa-alert',
  templateUrl: 'gpa-alert.component.html',
  styleUrls: ['gpa-alert.component.scss']
})

export class GpaAlertComponent extends AbstractComponent {

  /**
   * Текст на кнопке "Отмена"
   *
   * @type {string}
   */
  @Input() buttonCancelText: string = 'Отмена';

  /**
   * Текст на кнопке действия
   *
   * @type {string}
   */
  @Input() buttonActionText: string = 'Удалить';

  /**
   * Текст в заголовке
   */
  @Input() title: string = 'Удалить статью?';

  /**
   * Событие при клике на Действие (Удалить)
   *
   * @type {EventEmitter<boolean>}
   */
  @Output() actionClick: EventEmitter<boolean> = new EventEmitter<boolean>();

  /**
   * Событие при клике на Отменить
   *
   * @type {EventEmitter<boolean>}
   */
  @Output() cancelClick: EventEmitter<boolean> = new EventEmitter<boolean>();

  /**
   * Необходим для получение доступа к popup
   * И закрытия его
   */
  @ViewChild('popup') popup;

  constructor(protected el: ElementRef) {
    super(el);
  }

  action() {
    this.show();
    this.actionClick.emit(true);
  }

  cancel() {
    this.hide();
    this.cancelClick.emit(true);
  }

  /**
   * Закрытие alert
   */
  public hide() {
    this.popup.hide();
  }

  /**
   * Показать alert
   */
  public show() {
    this.popup.show();
  }

}
