import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GpaAlertComponent } from './gpa-alert.component';

describe('GpaAlertComponent', () => {
  let component: GpaAlertComponent;
  let fixture: ComponentFixture<GpaAlertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpaAlertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpaAlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
