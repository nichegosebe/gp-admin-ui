# GPA-CHECKBOX

Пример html кода

```angular2html
<gpa-checkbox
  text="WiRight"
  customId="123"
  [error]="false"
  [checked]="true"
></poz-checkbox>
```

- **text** - текст выводимый в компоненте
- **customId** - уникальный идентификатор
- **error** - текст ошибки {string | boolean}
- **checked** - значение по умолчанию (чекед или нет)
