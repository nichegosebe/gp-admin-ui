import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GpaCheckboxComponent } from './gpa-checkbox.component';

describe('GpaCheckboxComponent', () => {
  let component: GpaCheckboxComponent;
  let fixture: ComponentFixture<GpaCheckboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpaCheckboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpaCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
