import {Component, ElementRef, Input, OnInit} from '@angular/core';
import {AbstractComponent} from '../../../../abstract.component';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
  selector: 'gpa-checkbox',
  templateUrl: './gpa-checkbox.component.html',
  styleUrls: ['./gpa-checkbox.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: GpaCheckboxComponent,
      multi: true
    }
  ],
})
export class GpaCheckboxComponent extends AbstractComponent implements ControlValueAccessor, OnInit {

  private _id: string;

  /**
   * Уникальное значение
   */
  @Input() customId: string;

  /**
   * Текст компонента
   *
   * @type {string}
   */
  @Input() text: string = '';

  /**
   * Текст ошибки либо состояние ошибки
   *
   * @type {string | boolean}
   */
  @Input() error: string | boolean = false;

  /**
   * Выбрано или нет
   *
   * @type {boolean}
   */
  @Input() checked: boolean = false;

  constructor(protected elem: ElementRef) {
    super (elem);
  }

  ngOnInit() {
    this._id = 'checkbox_' + (new Date()).getTime();
  }

  writeValue(value: boolean): void {
    this.checked = value;
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  propagateChange = (_: any) => {
  };

  registerOnTouched(fn: any): void {
  }

  change(): void {
    this.propagateChange(this.checked);
  }

  get id(): string {
    return !!this.customId ? this.customId : this._id;
  }

  showErrorText(): boolean {
    return ((typeof this.error === 'string') && !!this.error);
  }

}
