import {Component, ElementRef, AfterViewChecked, Input, Output, EventEmitter, ChangeDetectorRef} from '@angular/core';
import {AbstractComponent} from '../../../../abstract.component';
import {NG_VALUE_ACCESSOR, ControlValueAccessor} from "@angular/forms";

@Component({
    selector: 'gpa-multiselect',
    templateUrl: './gpa-multiselect.component.html',
    styleUrls: ['./gpa-multiselect.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: GpaMultiselectComponent,
            multi: true
        }
    ],
    host: {
        '(document:click)': 'onDocumentClick($event)',
    },
})
export class GpaMultiselectComponent extends AbstractComponent implements ControlValueAccessor, AfterViewChecked {

    public hasSelected: boolean = false;
    public selected: any[] = [];
    public options: any[] = [];
    public opened: boolean = false;

    @Input() disabled: boolean = false;
    @Input() title: string;
    @Input() error: string;
    @Input() asObject: boolean;

    @Output() onScrollUp: EventEmitter<any> = new EventEmitter();
    @Output() onScrollDown: EventEmitter<any> = new EventEmitter();

    constructor(protected el: ElementRef, private changeDetector: ChangeDetectorRef) {
        super(el);
    }

    public onDocumentClick(e: Event) {
        if (!this.getElementRef().nativeElement.contains(e.target)) {
            this.opened = false;
        }
    }

    ngAfterViewChecked(): void {
        let opts = this.getElementRef().nativeElement.querySelectorAll('option');
        if (opts.length > 0) {
            let needRefresh: boolean = false;
            for (let opt of opts) {
                let exists = this.options.find(f => f.value == opt.value);
                if (!exists) {
                    this.addOption(opt.value, opt.text, opt.selected);
                    needRefresh = true;
                }
            }
            if (needRefresh) {
                this.hasSelected = this.isHasSelected();
                this.changeDetector.detectChanges();
                console.log(this.options);
            }
        }
    }

    propagateChange = (_: any) => {
    };

    writeValue(_value: string|number|any): void {
        if (!Array.isArray(_value)) {
            _value = _value ? [_value] : [];
        }
        let i: number;
        for (i = 0; i < _value.length; i++) {
            let el = _value[i];
            if (el && (typeof el === 'object')) {
                let keys = Object.keys(el);
                let j;
                for (j = 0; j < keys.length; j++) {
                    let k = keys[j];
                    let founded = this.options.find(f => f.value == k);
                    if (founded) {
                        founded.isSelected = true;
                    } else {
                        this.addOption(k, el[k], true, true);
                    }
                }
            } else {
                let founded = this.options.find(f => f.value == el);
                if (founded) {
                    founded.isSelected = true;
                    if (!this.isSelected(founded.value)) {
                        this.addSelected(founded.value, founded.title, false);
                    }
                }
            }
        }
        this.hasSelected = this.isHasSelected();
    }

    registerOnChange(fn: any): void {
        this.propagateChange = fn;
    }

    registerOnTouched(fn: any): void {
    }

    setDisabledState(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }

    toggleOpen(e): void {
        if (!this.disabled) {
            if (!this.opened || (this.opened && !e.target.closest('input') && !e.target.closest('a'))) {
                this.opened = !this.opened;
            }
        }
    }

    scrollDown(): void {
        this.onScrollDown.emit();
    }

    scrollUp(): void {
        this.onScrollUp.emit();
    }

    changeOption(item: any): void {
        let founded = this.options.find(f => f.value == item.value);
        if (founded) {
            founded.isSelected = !item.isSelected;
            if (founded.isSelected && !this.isSelected(founded.value)) {
                this.addSelected(founded.value, founded.title, false);
            } else if (!founded.isSelected && this.isSelected(founded.value)) {
                this.removeSelected(founded.value);
            }
            this.hasSelected = this.isHasSelected();
            this.notifyModel();
        }
    }

    // removeOption(item: any): void {
    //     let index: number = this.options.findIndex(f => f.value == item.value);
    //     if (index >= 0) {
    //         this.options[index].isSelected = false;
    //     }
    //     this.removeSelected(item.value);
    //     this.hasSelected = this.isHasSelected();
    //     this.notifyModel();
    // }

    private addOption(value: string|number, title: string|number, isSelected: boolean, isPseudo: boolean = false) {
        let exists = this.options.find(f => f.value == value);
        isSelected = isSelected || this.isSelected(value);
        if (!exists) {
            this.options.push({value: value, title: title, isSelected: isSelected, isPseudo: isPseudo});
        } else {
            exists.isSelected = isSelected;
        }
        if (isSelected && !this.isSelected(value)) {
            this.addSelected(value, title, isPseudo);
        }
    }

    private addSelected(value: string|number, title: string|number, isPseudo: boolean = false) {
        let exists = this.selected.find(f => f.value == value);
        if (!exists) {
            this.selected.push({value: value, title: title, isSelected: true, isPseudo: isPseudo});
        }
    }

    private removeSelected(value: string|number) {
        let index: number = this.selected.findIndex(f => f.value == value);
        if (index >= 0) {
            this.selected.splice(index, 1);
        }
    }

    private isSelected(value: string|number): boolean {
        let exists = this.selected.find(f => f.value == value);
        return !!exists;
    }

    private isHasSelected(): boolean {
        return (Object.keys(this.selected).length > 0);
    }

    private notifyModel(): void {
        const result: Object = {};
        let i: number;
        for (i = 0; i < this.selected.length; i++) {
            const el = this.selected[i];
            result[el.value] = el.title;
        }
        this.propagateChange(this.asObject ? result : Object.keys(result));
    }

    showErrorText(): boolean {
        return ((typeof this.error === 'string') && !!this.error);
    }
}
