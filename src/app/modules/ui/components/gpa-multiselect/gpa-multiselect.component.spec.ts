import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GpaMultiselectComponent } from './gpa-multiselect.component';

describe('GpaMultiselectComponent', () => {
  let component: GpaMultiselectComponent;
  let fixture: ComponentFixture<GpaMultiselectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpaMultiselectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpaMultiselectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
