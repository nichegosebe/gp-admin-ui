import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GpaPopupComponent } from './gpa-popup.component';

describe('GpaPopupComponent', () => {
  let component: GpaPopupComponent;
  let fixture: ComponentFixture<GpaPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpaPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpaPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
