import {Component, ElementRef, Output, EventEmitter} from '@angular/core';
import {AbstractComponent} from '../../../../abstract.component';

@Component({
    selector: 'gpa-popup',
    templateUrl: './gpa-popup.component.html',
    styleUrls: ['./gpa-popup.component.scss']
})

export class GpaPopupComponent extends AbstractComponent {

    @Output() onShow: EventEmitter<any> = new EventEmitter();
    @Output() onHide: EventEmitter<any> = new EventEmitter();

    /**
     * Индикатор показа всплывающего окна
     *
     * @type {boolean}
     */
    isShow: boolean = false;

    constructor(protected el: ElementRef) {
        super(el);
    }

    /**
     * Для закрытия
     */
    hide() {
        this.isShow = false;
        this.onHide.emit();
    }

    /**
     * Для показа
     */
    show() {
        this.isShow = true;
        this.onShow.emit();
    }

    /**
     * Переключатель
     */
    toogle() {
        this.isShow ? this.hide() : this.show();
    }

}
