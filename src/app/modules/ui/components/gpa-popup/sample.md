# GPA-POPUP

Пример html кода

```angular2html
<gpa-popup>
  <h1>Form</h1>
  <button></button>
</gpa-popup>
```

Все, что размещено между <gpa-popup> и </gpa-popup> будет
отображено в всплывающем окне.
Все функции кнопок, должны быть описаны в *.ts файле того компонента,
который вызывает popup.

По умолчанию, уведомление появляется сразу.

Для доступа к всплвающему окну необходимо использовать следующее

```angularjs
@ViewChild('popup') popup;
```

И вызвать метод `hide` для закрытия, или `show` для показа
