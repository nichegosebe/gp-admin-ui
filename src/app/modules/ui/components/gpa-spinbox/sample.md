# GPA-SPIN-BOX

#### Пример входных данных
```typescript
let value: number = 0;
let minValue: number = 0;
let maxValue: number = 100;
```

#### Пример html кода
```angular2html
<ns-spin-box

    prefix="от"
    prefixIcon="/assets/img/camera.svg"
    postfix="соток"
    postfixIcon="/assets/img/camera.svg"
    
    [maxValue]="maxValue"
    [minValue]="minValue"
    (ngModelChange)="function()"
    ([ngModel])="value">
</ns-spin-box>
```

- **type** - тип компонента (по его логике - number)
- **minValue** - минимальное значение для компонента (см. пример _minValue_ выше)
- **maxValue** - максимальное значение для компонента (см. пример _maxValue_ выше)
- **ngModelChange** - возвращает текущее значение (число)
- **ngModel** - устанавливает значение по умолчанию (число)

> ##### В будущем
> - **prefix** - Текст, который будет указан в начале компонента ("от", "до")
> - **prefixIcon** - Путь к картинке, которая будет отображена в начале компонента
> (Если указан prefix, не желательно указывать prefixIcon, и наоборот)
> - **postfix** - Текст, который будет указан в конце компонента ("соток", "м")
> - **postfixIcon** - Путь к картинке, которая будет отображена в конце компонента
> (Если указан postfix, не желательно указывать postfixIcon, и наоборот)
