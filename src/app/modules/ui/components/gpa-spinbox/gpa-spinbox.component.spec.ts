import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GpaSpinboxComponent } from './gpa-spinbox.component';

describe('GpaSpinboxComponent', () => {
  let component: GpaSpinboxComponent;
  let fixture: ComponentFixture<GpaSpinboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpaSpinboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpaSpinboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
