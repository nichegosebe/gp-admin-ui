import {Component, ElementRef, HostListener, Input} from '@angular/core';
import {AbstractComponent} from '../../../../abstract.component';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";
import {HelperService} from "../../../../services/helper.service";

@Component({
  selector: 'gpa-spinbox',
  templateUrl: './gpa-spinbox.component.html',
  styleUrls: ['./gpa-spinbox.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: GpaSpinboxComponent,
      multi: true
    }
  ]
})

export class GpaSpinboxComponent extends AbstractComponent implements ControlValueAccessor {

  private static readonly KEY_CODE_UP = 38;
  private static readonly KEY_CODE_DOWN = 40;
  private static readonly ALLOW_KEYS = [46, 8, 9, 27, 13, 110, 190];

  /**
   * Заголовок компонента
   */
  @Input() title: string;

  /**
   * Подсказка до ввода
   *
   * @type {string}
   */
  @Input() placeholder: string;

  /**
   * Шаг, на который будет изменяться знаечние
   *
   * @type {number}
   */
  @Input() step: number = 1;

  /**
   * Минимальное значение
   *
   * @type {number}
   */
  @Input() minValue: number | boolean = false;

  /**
   * Максимальное значение
   *
   * @type {number}
   */
  @Input() maxValue: number | boolean = false;

  /**
   * Текущее значение
   *
   * @type {number}
   */
  @Input() value: number = +this.minValue || 0;

  /**
   * Состояние доступности компонента
   *
   * @type {boolean}
   */
  @Input() disabled = false;

  /**
   * Текст ошибки
   *
   * @type {string | boolean}
   */
  @Input() error: string | boolean = false;

  // TODO: когда появится верстка, и появится ли вообще
  @Input() prefix: string | boolean = false;
  @Input() prefixIcon: string | boolean = false;
  @Input() postfix: string | boolean = false;
  @Input() postfixIcon: string | boolean = false;

  // TODO: ---------------------------------------------

  constructor(protected el: ElementRef,
              private helperService: HelperService) {
    super(el);
  }

  @HostListener('keydown', ['$event'])
  onKeyDown(event) {
    const e = <KeyboardEvent> event;

    if (GpaSpinboxComponent.ALLOW_KEYS.indexOf(e.keyCode) !== -1 ||
      // Allow: Ctrl+A
      (e.keyCode === 65 && e.ctrlKey === true) ||
      // Allow: Ctrl+C
      (e.keyCode === 67 && e.ctrlKey === true) ||
      // Allow: Ctrl+V
      (e.keyCode === 86 && e.ctrlKey === true) ||
      // Allow: Ctrl+X
      (e.keyCode === 88 && e.ctrlKey === true) ||
      // Allow: home, end, left, right
      (e.keyCode >= 35 && e.keyCode <= 39)) {
      // let it happen, don't do anything
      return;
    }

    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
      e.preventDefault();
    }
  }

  writeValue(value: number): void {
    if (this.validateValue(value)) {
      this.value = value;
    }
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  propagateChange = (_: any) => {
  };

  registerOnTouched(fn: any): void {
  }

  setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
  }

  /**
   * При вводе нажатии клавиши внутри html-элемента input
   *
   * @param {KeyboardEvent} event
   */
  onInputKeyDown(event: KeyboardEvent) {
    const keyCode = event.which || event.keyCode;

    if (keyCode === GpaSpinboxComponent.KEY_CODE_DOWN) {
      event.preventDefault();
      this.clickArrowDown();
    } else if (keyCode === GpaSpinboxComponent.KEY_CODE_UP) {
      event.preventDefault();
      this.clickArrowUp();
    }
  }

  /**
   * Проверяем и запрещаем вводить символов больше, чем указано
   * Если вводят больше, чем можно, устанавливаем максимальное
   *
   * @param {KeyboardEvent} event
   */
  onKeyPress(event: KeyboardEvent): void {
    if (this.value
      && (String(this.value).length === String(this.maxValue).length)
      && (!(!!this.helperService.getSelectedText()))) {
      event.preventDefault();
      if (this.value < +this.maxValue) {
        this.value = +this.maxValue;
      }
    }
  }

  /**
   * При вводе данных
   */
  change(): void {
    this.propagateChange(this.value);
  }

  showErrorText(): boolean {
    return ((typeof this.error === 'string') && !!this.error);
  }

  /**
   * Клик по стрелочке "вверх"
   */
  clickArrowUp() {
    const newVal = +this.value + this.step;

    if (this.maxValue === false || newVal <= this.maxValue) {
      this.value = newVal;
      this.propagateChange(newVal);
    }
  }

  /**
   * Клик по стрелочке "вниз"
   */
  clickArrowDown() {
    const newVal = +this.value - this.step;

    if (this.minValue === false || newVal >= this.minValue) {
      this.value = newVal;
      this.propagateChange(newVal);
    }
  }

  /**
   * При потере фокуса
   * выставлять в поле минимальное значение
   */
  inputBlur(): void {
    this.checkValue();
  }

  private validateValue(value: number): boolean {
    return ((this.maxValue === false) || (value <= this.maxValue))
      && ((this.minValue === false) || (value >= this.minValue));
  }

  /**
   * При вставке значений в компонент
   *
   * Нужна небольшая задержка, для получения данных после вставки
   */
  onPaste(): void {
    setTimeout(() => {
      this.checkValue();
    }, 1);
  }

  /**
   * Проверка значения при вставке | вводе с клавиатуры
   */
  checkValue() {
    this.value = Number(this.value);

    if (String(this.value).length === 0) {
      this.value = +this.minValue;
    }

    if (String(this.value).length > String(this.maxValue).length) {
      this.value = +this.maxValue;
    }
  }

}
