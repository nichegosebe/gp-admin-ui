export class AvatarUploadModel {
  private _avatarGuid: string;
  private _selectedFile: File;
  private _selectedFileSrc: string;
  private _isDeleted: boolean;

  get avatarGuid(): string {
    return this._avatarGuid;
  }

  set avatarGuid(value: string) {
    this._avatarGuid = value;
  }

  get selectedFile(): File {
    return this._selectedFile;
  }

  set selectedFile(value: File) {
    this._selectedFile = value;
  }

  get selectedFileSrc(): string {
    return this._selectedFileSrc;
  }

  set selectedFileSrc(value: string) {
    this._selectedFileSrc = value;
  }

  get isDeleted(): boolean {
    return this._isDeleted;
  }

  set isDeleted(value: boolean) {
    this._isDeleted = value;

    if (value) {
      this._selectedFile = null;
      this._selectedFileSrc = null;
    }
  }
}
