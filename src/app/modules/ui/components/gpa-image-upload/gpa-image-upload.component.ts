import {Component, EventEmitter, ElementRef, Input, OnInit, Output} from '@angular/core';
import {AvatarUploadModel} from './gpa-image-upload.model';
import {DomSanitizer, SafeStyle} from '@angular/platform-browser';

// import {filesize} from 'humanize';

import {AbstractComponent} from '../../../../abstract.component';

@Component({
    selector: 'gpa-image-upload',
    templateUrl: './gpa-image-upload.component.html',
    styleUrls: ['./gpa-image-upload.component.scss']
})

export class GpaImageUploadComponent extends AbstractComponent implements OnInit {

    public static LABEL_ADD_PHOTO = 'Выберите фотографию или перетащите сюда';
    public static LABEL_CHANGE_PHOTO = 'Изменить фотографию';

    /**
     * Текст ошибки
     */
    error: string;

    /**
     * GUID аватарки пользователя.
     */
    @Input() public initialAvatarGuid: string;

    /**
     * Текст с требованиями для загрузки файла
     */
    @Input() public inputRequirementsText: string;

    /**
     * Список разрешенных MIME-типов
     */
    @Input() public mimeTypes: Array<string>|string = ['image/png', 'image/jpeg'];

    /**
     * Максимальный размер файла в байтах
     */
    @Input() public maxSize: number;

    /**
     * Событие изменения модели
     *
     * @type {EventEmitter}
     */
    @Output() public modelChange: EventEmitter<AvatarUploadModel> = new EventEmitter<AvatarUploadModel>();

    /**
     * Модель компонента
     *
     * @type {AvatarUploadModel}
     * @private
     */
    private model: AvatarUploadModel = new AvatarUploadModel();

    constructor(private domSanitizer: DomSanitizer,
                protected el: ElementRef) {
        super(el);
    }

    /**
     * При инициализации компонента, загрузим в модель GUID аватарки
     */
    ngOnInit() {
        this.model.avatarGuid = this.initialAvatarGuid;
    }

    /**
     * Файл в инпуте изменен
     *
     * @param e
     */
    onChange(e) {
        const el = e.srcElement || e.target;
        const file = el.files.length > 0 ? el.files[0] : null;

        if (file) {
            this.updateModel(file);
        }
    }

    private updateModel(file: File) {
        this.error = '';

        if (this.mimeTypes.indexOf(file.type) === -1) {
            this.error = 'Неверный тип файла';
        } else if (!!this.maxSize && (file.size > this.maxSize)) {
            const pretty = this.maxSize; //filesize(this.maxSize);
            this.error = `Размер превышает ${pretty}`;
        } else {
            this.model.selectedFile = file;
            this.model.selectedFileSrc = URL.createObjectURL(file);
            this.model.isDeleted = false;
            this.modelChange.emit(this.model);
        }
    }

    /**
     * Нажата кнопка удаления файла
     */
    public onDelete(e: Event) {
        e.preventDefault();

        this.model.isDeleted = true;
        this.model.selectedFile = null;
        this.model.selectedFileSrc = null;
        this.modelChange.emit(this.model);
    }

    /**
     * Путь к картинке
     *
     * @returns {SafeStyle}
     */
    get avatarSrc(): SafeStyle {
        const src = this.model.selectedFile ? this.model.selectedFileSrc : '';

        return this.domSanitizer.bypassSecurityTrustStyle('url(' + src + ')');
    }

    /**
     * Проверяет наличие файла
     *
     * @returns {boolean}
     */
    get hasFile(): boolean {
        return !this.model.isDeleted
            && (!!this.model.avatarGuid || !!this.model.selectedFileSrc);
    }

    /**
     * Вывод пояснительной надписи
     *
     * @returns {string}
     */
    get inputLabel(): string {
        return this.hasFile ? GpaImageUploadComponent.LABEL_CHANGE_PHOTO : GpaImageUploadComponent.LABEL_ADD_PHOTO;
    }

    /**
     * Получить разрешенные типы
     *
     * @returns {string}
     */
    get acceptMimeTypes(): string {
        if (this.mimeTypes instanceof Array) {
            return this.mimeTypes.join(',');
        } else {
            return this.mimeTypes;
        }
    }
}
