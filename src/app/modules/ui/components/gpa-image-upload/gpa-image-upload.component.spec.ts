import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GpaImageUploadComponent } from './gpa-image-upload.component';

describe('GpaImageUploadComponent', () => {
  let component: GpaImageUploadComponent;
  let fixture: ComponentFixture<GpaImageUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpaImageUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpaImageUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
