import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GpaTextareaComponent } from './gpa-textarea.component';

describe('GpaTextareaComponent', () => {
  let component: GpaTextareaComponent;
  let fixture: ComponentFixture<GpaTextareaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpaTextareaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpaTextareaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
