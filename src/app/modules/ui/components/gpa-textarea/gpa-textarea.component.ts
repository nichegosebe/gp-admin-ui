import {Component, ElementRef, Input} from '@angular/core';
import {AbstractComponent} from '../../../../abstract.component';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";

@Component({
  selector: 'gpa-textarea',
  templateUrl: './gpa-textarea.component.html',
  styleUrls: ['./gpa-textarea.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: GpaTextareaComponent,
      multi: true
    }
  ],
})

export class GpaTextareaComponent extends AbstractComponent implements ControlValueAccessor {

  /**
   * Высота поля в строках текста
   *
   * @type {number}
   */
  @Input() rows: number;

  /**
   * Ширина поля в символах
   *
   * @type {number}
   */
  @Input() cols: number;

  /**
   * Имя поля, предназначено для того, чтобы обработчик формы мог его идентифицировать
   *
   * @type {string}
   */
  @Input() name: string = '';

  /**
   * Заголовок компонента
   *
   * @type {string}
   */
  @Input() title: string = '';

  /**
   * Текст ошибки
   *
   * @type {boolean}
   */
  @Input() error: string | boolean = false;

  /**
   * Текущее значение
   *
   * @type {string}
   */
  @Input() value: string;

  /**
   * Состояние компонента
   *
   * @type {boolean}
   */
  @Input() disabled: boolean = false;

  //TODO: если добавят
  @Input() maxLength: number;

  constructor(protected el: ElementRef) {
    super(el);
  }

  writeValue(value: string): void {
    this.value = value;
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  propagateChange = (_: any) => {
  };

  registerOnTouched(fn: any): void {
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  change(): void {
    this.propagateChange(this.value);
  }

  showErrorText(): boolean {
    return ((typeof this.error === 'string') && !!this.error);
  }

}
