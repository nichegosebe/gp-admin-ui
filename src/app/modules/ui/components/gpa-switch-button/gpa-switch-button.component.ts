import {Component, ElementRef, Input} from '@angular/core';
import {AbstractComponent} from '../../../../abstract.component';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
  selector: 'gpa-switch-button',
  templateUrl: './gpa-switch-button.component.html',
  styleUrls: ['./gpa-switch-button.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: GpaSwitchButtonComponent,
      multi: true
    }
  ]
})

export class GpaSwitchButtonComponent extends AbstractComponent implements ControlValueAccessor {

  /**
   * Текст внутри кнопки
   *
   * @type {string}
   */
  @Input() text: string = '';

  /**
   * Состояние (стили)
   *
   * @type {string}
   */
  @Input() state: string;

  /**
   * Стиль для размера
   *
   * @type {string}
   */
  @Input() size: string;

  /**
   * Чекед или нет
   *
   * @type {boolean}
   */
  @Input() checked: boolean = false;

  /**
   * Состояние доступности компонента
   *
   * @type {boolean}
   */
  @Input() disabled: boolean = false;

  constructor(protected el: ElementRef) {
    super(el);
  }

  toggleClass() {
    this.checked = !this.checked;
    this.propagateChange(this.checked);
  }

  writeValue(value: boolean): void {
    this.checked = value;
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  propagateChange = (_: any) => {
  };

}
