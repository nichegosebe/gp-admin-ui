import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GpaSwitchButtonComponent } from './gpa-switch-button.component';

describe('GpaSwitchButtonComponent', () => {
  let component: GpaSwitchButtonComponent;
  let fixture: ComponentFixture<GpaSwitchButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpaSwitchButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpaSwitchButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
