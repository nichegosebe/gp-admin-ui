import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GpaSelectComponent } from './gpa-select.component';

describe('GpaSelectComponent', () => {
  let component: GpaSelectComponent;
  let fixture: ComponentFixture<GpaSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpaSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpaSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
