import {
    Component, ElementRef, Input, Output, EventEmitter, NgZone, ChangeDetectorRef,
    AfterViewChecked
} from '@angular/core';
import {AbstractComponent} from '../../../../abstract.component';
import {NG_VALUE_ACCESSOR, ControlValueAccessor} from "@angular/forms";

@Component({
    selector: 'gpa-select',
    templateUrl: './gpa-select.component.html',
    styleUrls: ['./gpa-select.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: GpaSelectComponent,
            multi: true
        }
    ],
    host: {
        '(document:click)': 'onDocumentClick($event)',
    },
})

export class GpaSelectComponent extends AbstractComponent implements ControlValueAccessor, AfterViewChecked {
    @Input() title: string = '';
    @Input() error: string = '';
    @Input() disabled: boolean = false;
    @Input() placeholder: string = '';
    @Input() value: string|number|any;

    @Output() onScrollUp: EventEmitter<any> = new EventEmitter();
    @Output() onScrollDown: EventEmitter<any> = new EventEmitter();

    public options: any[] = [];
    public opened: boolean = false;
    public valueTitle: string = '';

    private isObject: boolean = false;

    constructor(private elRef: ElementRef, private changeDetector: ChangeDetectorRef, private zone: NgZone) {
        super(elRef);
    }

    public onDocumentClick(e: Event) {
        if (!this.getElementRef().nativeElement.contains(e.target)) {
            this.opened = false;
        }
    }

    ngAfterViewChecked(): void {
        this.zone.runOutsideAngular(() => {
            const opts = this.getElementRef().nativeElement.querySelectorAll('option');
            if (opts.length > 0) {
                let newOpts = [],
                    replaceOptions: boolean = (this.options.length != opts.length);

                for (let opt of opts) {
                    newOpts.push({key: opt.value, value: opt.text});
                    if (!replaceOptions) {
                        replaceOptions = (this.getOptionByKey(opt.value) === null);
                    }
                }

                if (replaceOptions) {
                    this.options = newOpts;
                    this.valueTitle = this.getTitleByValue(this.value);
                    this.changeDetector.detectChanges();
                }
            }
        });
    }

    showErrorText(): boolean {
        return ((typeof this.error === 'string') && !!this.error);
    }

    propagateChange = (_: any) => {
    };

    writeValue(_value: string|number|any): void {
        if (_value && (typeof _value === 'object')) {
            this.isObject = true;
            this.value = _value.key;
        } else {
            this.isObject = false;
            this.value = _value;
        }
        this.valueTitle = this.getTitleByValue(_value);
    }

    registerOnChange(fn: any): void {
        this.propagateChange = fn;
    }

    registerOnTouched(fn: any): void {
    }

    setDisabledState(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }

    setValue(_value: string|number): void {
        if (!this.disabled) {
            let title = this.getTitleByValue(_value);
            this.value = this.isObject ? {key: _value, value: title} : _value;
            this.valueTitle = title;
            this.propagateChange(this.value);
        }
    }

    scrollDown(): void {
        this.onScrollDown.emit();
    }

    scrollUp(): void {
        this.onScrollUp.emit();
    }

    private getTitleByValue(_value: string|number|any): string {
        let result = null;
        if (_value && (typeof _value === 'object')) {
            result = _value.value;
        } else {
            let opt = this.getOptionByKey(_value);
            if (opt) {
                result = opt.value;
            }
        }
        if (result === null) {
            result = this.valueTitle;
        }
        return result;
    }

    private getOptionByKey(_key: string|number): any {
        let result: any = null;
        for (let opt of this.options) {
            if (opt.hasOwnProperty('key') && opt.hasOwnProperty('value')) {
                if (opt.key == _key) {
                    result = opt;
                    break;
                }
            }
        }
        return result;
    }
}
