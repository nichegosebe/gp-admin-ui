import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GpaPhoneInputComponent } from './gpa-phone-input.component';

describe('GpaPhoneInputComponent', () => {
  let component: GpaPhoneInputComponent;
  let fixture: ComponentFixture<GpaPhoneInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpaPhoneInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpaPhoneInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
