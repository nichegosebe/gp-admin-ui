import {Component, ElementRef, HostListener, Input, OnInit, forwardRef} from '@angular/core';
import {AbstractComponent} from '../../../../abstract.component';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";

@Component({
    selector: 'gpa-phone-input',
    templateUrl: './gpa-phone-input.component.html',
    styleUrls: ['./gpa-phone-input.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => GpaPhoneInputComponent),
            multi: true
        }
    ],
})
export class GpaPhoneInputComponent extends AbstractComponent implements OnInit, ControlValueAccessor {

    /**
     * Состояние компонента
     * @type {boolean}
     */
    @Input() disabled: boolean = false;

    /**
     * Уникальное имя
     */
    @Input() name: string;

    /**
     * Плейсхолдер - для предпросмотра
     * @type {string}
     */
    @Input() placeholder: string = 'Телефон';

    /**
     * Текст ошибки
     */
    @Input() error: string;

    /**
     * Заголовок компонента
     * @type {string}
     */
    @Input() title: string = 'Телефон';

    /**
     * Телефон
     */
    @Input() phone: string;

    /**
     * Регулярное выражение
     * @type {regexp}
     */
    private static readonly PHONE_FORMAT_REGEXP: Array<any> = [
        {regexp: /^(\d)$/, format: '+$1'},
        {regexp: /^(\d)(\d{1,3})$/, format: '+$1 $2'},
        {regexp: /^(\d)(\d{3})(\d{1,3})$/, format: '+$1 $2 $3'},
        {regexp: /^(\d)(\d{3})(\d{3})(\d{1,2})$/, format: '+$1 $2 $3-$4'},
        {regexp: /^(\d)(\d{3})(\d{3})(\d{2})(\d{1,2})$/, format: '+$1 $2 $3-$4-$5'}
    ];

    constructor(protected el: ElementRef) {
        super(el);
    }

    @HostListener('keydown', ['$event'])
    onKeyDown(event) {
        const e = <KeyboardEvent> event;

        if ([46, 8, 9, 27, 13, 110, 190].indexOf(e.keyCode) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode === 65 && e.ctrlKey === true) ||
            // Allow: Ctrl+C
            (e.keyCode === 67 && e.ctrlKey === true) ||
            // Allow: Ctrl+V
            (e.keyCode === 86 && e.ctrlKey === true) ||
            // Allow: Ctrl+X
            (e.keyCode === 88 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }

        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    }

    ngOnInit() {
        if (this.phone) {
            this.prettyPhone(this.phone);
        }
    }

    writeValue(value: string): void {
        if (value) {
            this.prettyPhone(value);
        } else {
            this.phone = null;
        }
    }

    registerOnChange(fn: any): void {
        this.propagateChange = fn;
    }

    propagateChange = (_: any) => {
    };

    registerOnTouched(fn: any): void {
    }

    setDisabledState(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }

    /**
     * При вводе чисел
     * @param e
     */
    onChange(e) {
        this.prettyPhone(e);
    }

    /**
     * Преобразование телефона
     * @param phone
     */
    private prettyPhone(phone): void {
        phone = phone.replace(/\D/g, '');

        for (let i = 0; i < GpaPhoneInputComponent.PHONE_FORMAT_REGEXP.length; i++) {
            const regexp = GpaPhoneInputComponent.PHONE_FORMAT_REGEXP[i].regexp;
            const format = GpaPhoneInputComponent.PHONE_FORMAT_REGEXP[i].format;
            if (regexp.test(phone)) {
                phone = phone.replace(regexp, format);
                break;
            }
        }

        this.phone = phone;
        this.propagateChange(this.phone);
    }

}
