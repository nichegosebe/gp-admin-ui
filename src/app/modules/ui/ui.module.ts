import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {BrowserModule} from '@angular/platform-browser';
import {HttpModule} from '@angular/http';
import {DragulaModule} from 'ng2-dragula';
import {TinymceModule} from 'angular2-tinymce';

import {GpaScrollDirective} from './directives/gpa-scroll/gpa-scroll.directive';
import {GpaDDSortDirective} from './directives/gpa-dd-sort/gpa-dd-sort.directive';

import {GpaCheckboxComponent} from './components/gpa-checkbox/gpa-checkbox.component';
import {GpaAlertComponent} from './components/gpa-alert/gpa-alert.component';
import {GpaSpinboxComponent} from './components/gpa-spinbox/gpa-spinbox.component';
import {GpaSelectComponent} from './components/gpa-select/gpa-select.component';
import {GpaInputComponent} from './components/gpa-input/gpa-input.component';
import {GpaTextareaComponent} from './components/gpa-textarea/gpa-textarea.component';
import {GpaWysiwygComponent} from './components/gpa-wysiwyg/gpa-wysiwyg.component';
import {GpaImageUploadComponent} from './components/gpa-image-upload/gpa-image-upload.component';
import {GpaYandexMapComponent} from './components/gpa-yandex-map/gpa-yandex-map.component';
import {GpaPhoneInputComponent} from './components/gpa-phone-input/gpa-phone-input.component';
import {GpaButtonComponent} from './components/gpa-button/gpa-button.component';
import {GpaSwitchButtonComponent} from './components/gpa-switch-button/gpa-switch-button.component';
import {GpaRadioButtonComponent} from './components/gpa-radio-button/gpa-radio-button.component';
import {GpaMultiselectComponent} from './components/gpa-multiselect/gpa-multiselect.component';
import {GpaPopupComponent} from './components/gpa-popup/gpa-popup.component';
import {GpaUiSampleComponent} from './gpa-ui-sample/gpa-ui-sample.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BrowserModule,
    HttpModule,
    DragulaModule,
    TinymceModule
  ],
  declarations: [
    GpaAlertComponent,
    GpaSpinboxComponent,
    GpaSelectComponent,
    GpaInputComponent,
    GpaTextareaComponent,
    GpaWysiwygComponent,
    GpaImageUploadComponent,
    GpaYandexMapComponent,
    GpaPhoneInputComponent,
    GpaButtonComponent,
    GpaSwitchButtonComponent,
    GpaRadioButtonComponent,
    GpaMultiselectComponent,
    GpaPopupComponent,
    GpaScrollDirective,
    GpaDDSortDirective,
    GpaCheckboxComponent,
    GpaUiSampleComponent
  ],
  exports: [
    GpaAlertComponent,
    GpaSpinboxComponent,
    GpaSelectComponent,
    GpaInputComponent,
    GpaTextareaComponent,
    GpaWysiwygComponent,
    GpaImageUploadComponent,
    GpaYandexMapComponent,
    GpaPhoneInputComponent,
    GpaButtonComponent,
    GpaSwitchButtonComponent,
    GpaRadioButtonComponent,
    GpaMultiselectComponent,
    GpaPopupComponent,
    GpaCheckboxComponent,
    GpaScrollDirective,
    GpaDDSortDirective,
    GpaUiSampleComponent
  ]
})

export class UiModule {
}
