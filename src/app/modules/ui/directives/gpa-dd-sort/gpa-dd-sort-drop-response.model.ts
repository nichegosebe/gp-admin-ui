/**
 * Модель, описывающая формат события "drop"
 */
export class DDSortDropResponse {
    bagName: string;
    el: any;
    target: any;
    source: any;
    sibling: any;
}
