import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GpaUiSampleComponent } from './gpa-ui-sample.component';

describe('GpaUiSampleComponent', () => {
  let component: GpaUiSampleComponent;
  let fixture: ComponentFixture<GpaUiSampleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpaUiSampleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpaUiSampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
