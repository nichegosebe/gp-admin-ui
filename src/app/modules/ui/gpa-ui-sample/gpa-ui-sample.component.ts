import {Component, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'gpa-ui-sample',
  templateUrl: './gpa-ui-sample.component.html',
  styleUrls: ['./gpa-ui-sample.component.scss']
})

export class GpaUiSampleComponent implements OnInit {

  multiSelectedItems: any[] = [3, 5, 7];
  selectedItem: number = 5;

  @ViewChild('popup') popup;
  @ViewChild('alert') alert;

  constructor() {
  }

  ngOnInit() {
  }

  getMultiSelectItems(): any[] {
    const result = [];
    for (let i = 0; i < 10; i++) {
      result.push({value: i, title: 'Элемент ' + i});
    }
    return result;
  }

  showAlert() {
    console.log('Alert OK');
  }

  getSelectItems(): any[] {
    const result = [];
    for (let i = 0; i < 10; i++) {
      result.push({id: i, title: 'Элемент ' + i});
    }
    return result;
  }

  /**
   * Функция для кнопки в popup
   * Кнопка "Подтвердить"
   */
  popupConfirm() {
    console.log('Popup Confirm!');
  }

  /**
   * Функция для кнопки в popup
   * Кнопка "Закрыть"
   */
  popupClose() {
    console.log('Popup Close!');
    this.popup.hide();
  }

  click() {
    this.alert.show();
  }

  //TODO: пример
  actionClick() {
    console.log('action click!');
  }

  //TODO: пример
  cancelClick() {
    console.log('cancel click');
  }

}
